package com.example.testwebview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.WebView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val webView = findViewById<WebView>(R.id.webView);
        val btn = findViewById<FloatingActionButton>(R.id.fab)
        webView.loadUrl("file:///android_asset/test.html");
        btn.setOnClickListener {
            Log.d("TAG", "Click")
            webView.scrollTo(webView.scrollX + 200, 0)
        }
    }
}